SkyAndroidTest is an APP developed to apply for SKY Native Android Job.

Requirements: https://bitbucket.org/lunider/skyandroidtest/src/master/Requirements/

This App includes a List of Movies.

The list data is downloaded through SKY API hosted at heroku.com: https://sky-exercise.herokuapp.com/api/Movies

Following libs are used in this project:

	Spring Rest - HTTP requisitions
	Android Annotations - Clean Code and UI-Background threads
	OrmLite - SQLite database storage
	Picasso - Pictures download/view
	Jackson - Faster JSON converter
	
Compiled Apk is available at https://bitbucket.org/lunider/skyandroidtest/src/master/Apk/

App screenshots: https://bitbucket.org/lunider/skyandroidtest/src/master/Screenshots/
