package sky.teste.cinesky.Service;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.j256.ormlite.dao.Dao;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.ormlite.annotations.OrmLiteDao;
import org.androidannotations.rest.spring.annotations.RestService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sky.teste.cinesky.Cloud.SpringRest;
import sky.teste.cinesky.Model.Movie;
import sky.teste.cinesky.Util.ConnectivityUtil;
import sky.teste.cinesky.Util.DatabaseHelper;

@EBean
public class MovieService {

    @OrmLiteDao(helper = DatabaseHelper.class)
    Dao<Movie, Integer> movieDao;

    @RestService
    SpringRest springRest;

    @RootContext
    Context context;

    ArrayList<Movie> movies = new ArrayList<>();

    public synchronized List<Movie> getMovies() throws SQLException {
        ConnectivityUtil connectivityUtil = new ConnectivityUtil(context);
        if (connectivityUtil.hasNetworkConnection()) {
            movies = springRest.getMovies();
            persistMovies();
        }
        return movieDao.queryForAll();
    }

    private void persistMovies() {
        try {
            movieDao.delete(movieDao.queryForAll());
            for (Movie movie : movies) {
                movie = fixCoverImageSize(movie);
                movie = downloadCoverImage(movie);
                movieDao.createOrUpdate(movie);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Movie fixCoverImageSize(Movie movie) {
        String[] urlParts = movie.getCover_url().split("/");
        if (!urlParts[5].equals("w1280"))
            urlParts[5] = "w1280";
        movie.setCover_url(getJointString(urlParts));
        return movie;
    }

    private String getJointString(String[] strings) {
        StringBuilder result = new StringBuilder();
        for (String string : strings) {
            result.append(string);
            result.append("/");
        }
        result.delete(result.length() - 1, result.length());
        return result.toString();
    }

    private Movie downloadCoverImage(Movie movie){
        String movieLocalPath = Environment.getExternalStorageDirectory().getPath() + "/movies/" + movie.getId() + ".jpg";
        File file = new File(movieLocalPath);
        try {
            file.createNewFile();
            FileOutputStream ostream = new FileOutputStream(file);
            Picasso.get()
                    .load(movie.getCover_url()).get().compress(Bitmap.CompressFormat.JPEG, 100, ostream);
            ostream.flush();
            ostream.close();
            movie.setCover_url(movieLocalPath);
        } catch (IOException e) {
        }
        return movie;
    }
}
