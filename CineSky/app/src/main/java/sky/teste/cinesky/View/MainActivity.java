package sky.teste.cinesky.View;


import android.Manifest;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.security.Permission;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sky.teste.cinesky.Model.Movie;
import sky.teste.cinesky.R;
import sky.teste.cinesky.Service.MovieService;
import sky.teste.cinesky.Util.MsgUtil;
import sky.teste.cinesky.Util.PermissionsUtil;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.gallery)
    RecyclerView recyclerView;

    @ViewById(R.id.contentTextView)
    TextView contentTextView;

    @ViewById(R.id.logo)
    ImageView logo;

    @Bean
    MovieService movieService;

    GalleryAdapter galleryAdapter;

    public static final String[] permissions = {
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET
    };


    @AfterViews
    void afterViews() {
        getSupportActionBar().setTitle(R.string.cine_sky);
        configureRecyclerView();
        PermissionsUtil.requestPermissions(this, permissions, 123);
        loadData();
    }

    private void configureRecyclerView() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(
                getApplicationContext(), 2);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Background
    void loadData() {
        List<Movie> movies = new ArrayList<>();
        try {
            movies = movieService.getMovies();
        } catch (SQLException e) {
            showErrorMessage(getString(R.string.failed_to_load_content));
            e.printStackTrace();
        }
        configureAdapter(movies);
    }

    private void configureAdapter(List<Movie> movies) {
        galleryAdapter = new GalleryAdapter(this, movies);
        showContent();
    }

    @UiThread
    void showContent() {
        recyclerView.setAdapter(galleryAdapter);
        logo.setVisibility(View.GONE);
        contentTextView.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    @UiThread
    void showErrorMessage(String message) {
        MsgUtil.showToast(this, message);
    }

}
