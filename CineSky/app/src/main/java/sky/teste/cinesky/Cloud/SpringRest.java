package sky.teste.cinesky.Cloud;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.util.ArrayList;
import java.util.HashMap;

import sky.teste.cinesky.Model.Movie;

/**
 * Created by lunid on 25/01/2018.
 */

@Rest(rootUrl = "https://sky-exercise.herokuapp.com/",converters = { MappingJackson2HttpMessageConverter.class})
@Accept(MediaType.APPLICATION_JSON)
public interface SpringRest {

    @Get("api/Movies")
    ArrayList<Movie> getMovies();

}
