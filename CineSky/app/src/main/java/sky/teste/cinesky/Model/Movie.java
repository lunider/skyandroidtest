package sky.teste.cinesky.Model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable
public class Movie {

    @DatabaseField(generatedId = true)
    @Expose
    protected Integer movieId;

    @DatabaseField
    @JsonProperty("id")
    protected String id;

    @DatabaseField
    @JsonProperty("title")
    protected String title;

    @DatabaseField
    @JsonProperty("overview")
    protected String overview;

    @DatabaseField
    @JsonProperty("duration")
    protected String duration;

    @DatabaseField
    @JsonProperty("release_year")
    protected String release_year;

    @DatabaseField
    @JsonProperty("cover_url")
    protected String cover_url;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    @JsonProperty("backdrops_url")
    protected String[] backDrops;

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRelease_year() {
        return release_year;
    }

    public void setRelease_year(String release_year) {
        this.release_year = release_year;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String[] getBackDrops() {
        return backDrops;
    }

    public void setBackDrops(String[] backDrops) {
        this.backDrops = backDrops;
    }
}
