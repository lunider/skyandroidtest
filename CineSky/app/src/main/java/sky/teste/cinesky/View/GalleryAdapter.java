package sky.teste.cinesky.View;

import android.Manifest;
import android.content.Context;
import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.annotations.Expose;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import sky.teste.cinesky.Model.Movie;
import sky.teste.cinesky.R;
import sky.teste.cinesky.Util.PermissionsUtil;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private List<Movie> movies;
    private Context context;


    public GalleryAdapter(Context context, List<Movie> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gallery_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder viewHolder, int i) {
        viewHolder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (PermissionsUtil.hasPermission((MainActivity_) context, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            loadStorageImage(viewHolder, i);
        else
            loadCloudImage(viewHolder, i);

        viewHolder.movieTitle.setText(movies.get(i).getTitle());
    }

    private void loadStorageImage(final GalleryAdapter.ViewHolder viewHolder, int i) {
        File image = new File(movies.get(i).getCover_url());
        Picasso.get()
                .load(image)
                .into(viewHolder.imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        viewHolder.loadMovieProgressBar.clearAnimation();
                        viewHolder.loadMovieProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        viewHolder.loadMovieProgressBar.clearAnimation();
                        viewHolder.loadMovieProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void loadCloudImage(GalleryAdapter.ViewHolder viewHolder, int i) {
        Picasso.get()
                .load(movies.get(i).getCover_url())
                .error(R.drawable.sky)
                .placeholder(R.drawable.sky)
                .into(viewHolder.imageView);
    }


    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView movieTitle;
        private ProgressBar loadMovieProgressBar;

        public ViewHolder(View view) {
            super(view);
            movieTitle = view.findViewById(R.id.movieTitle);
            imageView = view.findViewById(R.id.imageView);
            loadMovieProgressBar = view.findViewById(R.id.loadMovieProgressBar);
        }
    }
}

